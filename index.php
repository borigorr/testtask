<?php
namespace testTask;
use testTask\CalcSales;
use testTask\products\FabricProduct;
use testTask\sales\SalesProductType;
use testTask\sales\SalesPoductCouple;
use testTask\sales\SalesProductCout;

spl_autoload_register(function ($class_name) {
    $class = str_replace('testTask\\', '', $class_name);
    $patch =  __DIR__ . '/' .  str_replace("\\", '/', $class) . ".php";
    include $patch;
});


$calcSale = new CalcSales();

$calcSale->setProduct(FabricProduct::getProductA(100));

$calcSale->setProduct(FabricProduct::getProductB(100));

$calcSale->setProduct(FabricProduct::getProductC(100));
$calcSale->setProduct(FabricProduct::getProductL(100));

$saleProductType1 = new SalesProductType();
$saleProductType1->setSale(10);
$saleProductType1->setProductType(FabricProduct::PRODUCT_A);
$saleProductType1->setProductType(FabricProduct::PRODUCT_B);
$calcSale->setSale($saleProductType1);

$saleProductType2 = new SalesProductType();
$saleProductType2->setSale(6);
$saleProductType2->setProductType(FabricProduct::PRODUCT_D);
$saleProductType2->setProductType(FabricProduct::PRODUCT_E);
$calcSale->setSale($saleProductType2);

$saleProductType3 = new SalesProductType();
$saleProductType3->setSale(6);
$saleProductType3->setProductType(FabricProduct::PRODUCT_G);
$saleProductType3->setProductType(FabricProduct::PRODUCT_E);
$saleProductType3->setProductType(FabricProduct::PRODUCT_F);
$calcSale->setSale($saleProductType3);

$saleCouple = new SalesPoductCouple();
$saleCouple->setSale(5);
$saleCouple->setProductType(FabricProduct::PRODUCT_A);
$saleCouple->setProductCouple(FabricProduct::PRODUCT_L);
$saleCouple->setProductCouple(FabricProduct::PRODUCT_M);
$saleCouple->setProductCouple(FabricProduct::PRODUCT_K);
$calcSale->setSale($saleCouple);

$saleCount = new SalesProductCout();
$saleCount->setSale(5);
$saleCount->setProductCount(3);
$saleCount->setIgnoreType(FabricProduct::PRODUCT_A);
$saleCount->setIgnoreType(FabricProduct::PRODUCT_C);
$calcSale->setSale($saleCount);

$saleCount2 = new SalesProductCout();
$saleCount2->setSale(10);
$saleCount2->setProductCount(4);
$saleCount2->setIgnoreType(FabricProduct::PRODUCT_A);
$saleCount2->setIgnoreType(FabricProduct::PRODUCT_C);
$calcSale->setSale($saleCount2);

$saleCount3 = new SalesProductCout();
$saleCount3->setSale(20);
$saleCount3->setProductCount(5);
$saleCount3->setIgnoreType(FabricProduct::PRODUCT_A);
$saleCount3->setIgnoreType(FabricProduct::PRODUCT_C);
$calcSale->setSale($saleCount3);

$calcSale->applySales();
echo $calcSale->getSumm();
?>