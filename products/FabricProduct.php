<?php
namespace testTask\products;

class FabricProduct{

    const PRODUCT_A = 'PRODUCT_A';
    const PRODUCT_B = 'PRODUCT_B';
    const PRODUCT_C = 'PRODUCT_C';
    const PRODUCT_D = 'PRODUCT_D';
    const PRODUCT_E = 'PRODUCT_E';
    const PRODUCT_F = 'PRODUCT_F';
    const PRODUCT_G = 'PRODUCT_G';
    const PRODUCT_H = 'PRODUCT_H';
    const PRODUCT_I = 'PRODUCT_I';
    const PRODUCT_J = 'PRODUCT_J';
    const PRODUCT_K = 'PRODUCT_K';
    const PRODUCT_L = 'PRODUCT_L';
    const PRODUCT_M = 'PRODUCT_M';

    public static function getProductA(float $price){
        return new Product(self::PRODUCT_A, $price);
    }

    public static function getProductB(float $price){
        return new Product( self::PRODUCT_B, $price);
    }

    public static function getProductC(float $price){
        return new Product( self::PRODUCT_C, $price);
    }

    public static function getProductD(float $price){
        return new Product(self::PRODUCT_D, $price);
    }

    public static function getProductE(float $price){
        return new Product(self::PRODUCT_E, $price);
    }

    public static function getProductF(float $price){
        return new Product(self::PRODUCT_F, $price);
    }

    public static function getProductG(float $price){
        return new Product(self::PRODUCT_G, $price);
    }

    public static function getProductH(float $price){
        return new Product(self::PRODUCT_I, $price );
    }

    public static function getProductI(float $price){
        return new Product(self::PRODUCT_H, $price);
    }

    public static function getProductJ(float $price){
        return new Product(self::PRODUCT_J, $price);
    }

    public static function getProductK(float $price){
        return new Product(self::PRODUCT_K, $price);
    }

    public static function getProductL(float $price){
        return new Product(self::PRODUCT_L, $price);
    }

    public static function getProductM(float $price){
        return new Product(self::PRODUCT_M, $price);
    }
}
?>