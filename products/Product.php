<?php

namespace testTask\products;

use testTask\interfaces\Product as interfaceProduct;

class Product implements interfaceProduct
{

    private $produceType;

    private $price;

    private $useSale = false;

    public function __construct(string $productType, float $price)
    {
        $this->produceType = $productType;
        $this->price = $price;
    }

    public function setProductType(string $productType)
    {
        $this->produceType = $productType;
    }

    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    public function setUseSales(bool $useSale)
    {
        $this->useSale = $useSale;
    }

    public function getProductType(): string
    {
        return $this->produceType;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getUseSales(): bool
    {
        return $this->useSale;
    }
}

?>