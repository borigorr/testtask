<?php

namespace testTask\sales;

use testTask\interfaces\Sales;

class SalesProductCout implements Sales
{
    static $useSalesCount = false;

    private $producCount = 0;

    private $sale = 0;

    private $productTypeIgnore = [];

    public function setSale(float $sale)
    {
        $this->sale = $sale;
    }

    public function calcPrice(array $products) : array
    {
        if (self::$useSalesCount){
            return $products;
        }

        $productKeys = [];

        foreach ($products as $key => $product){
            if (!$product->getUseSales() && !in_array($product->getProductType(), $this->productTypeIgnore)){
                $productKeys[] = $key;
            }
        }

        if (count($productKeys) && $this->producCount === count($productKeys)){
            self::$useSalesCount = true;
            foreach ($productKeys as $productKey) {
                $price = $products[$productKey]->getPrice();
                $products[$productKey]->setUseSales(true);
                $products[$productKey]->setPrice($price - ($price / 100 * $this->sale ));
            }
        }

        return $products;

    }

    public function setProductCount(int $count){

        $this->producCount = $count;
    }

    public function setIgnoreType(string $productType){
        $this->productTypeIgnore[] = $productType;
    }


}

?>