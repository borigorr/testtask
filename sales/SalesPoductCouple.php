<?php

namespace testTask\sales;

use testTask\interfaces\Sales;

class SalesPoductCouple implements Sales
{

    private $productType = '';

    private $coupleVariable = [];

    private $sale = 0;

    public function setSale(float $sale)
    {
        $this->sale = $sale;
    }

    public function calcPrice(array $products): array
    {
        $productKey = false;

        foreach ($products as $key => $product) {
            if (!$product->getUseSales() && $product->getProductType() === $this->productType ) {
                $productKey = $key;
                break;
            }
        }

        if ($productKey === false){
            return $products;
        }

        foreach ($products as $product) {
            if (!$product->getUseSales() && in_array($product->getProductType(), $this->coupleVariable) ) {
                $price = $products[$productKey]->getPrice();
                $products[$productKey]->setUseSales(true);
                $products[$productKey]->setPrice($price - ($price / 100 * $this->sale));
                break;
            }

        }

        return $products;

    }

    public function setProductType(string $productType)
    {

        $this->productType = $productType;
    }

    public function setProductCouple(string $productType)
    {
        $this->coupleVariable[] = $productType;
    }
}

?>