<?php

namespace testTask\sales;

use testTask\interfaces\Sales;

class SalesProductType implements Sales
{

    private $productsTypes = [];

    private $sale = 0;

    public function setSale(float $sale)
    {
        $this->sale = $sale;
    }

    public function calcPrice(array $products) : array
    {
        $productsWithSales = [];

        foreach ($products as $key => $product){
            if (!$product->getUseSales() && in_array($product->getProductType(), $this->productsTypes)){
                $productsWithSales[] = $key;
            }
        }

        if (count($this->productsTypes) === count($productsWithSales)){
            foreach ($productsWithSales as $productIndex){
                $price = $products[$productIndex]->getPrice();
                $products[$productIndex]->setUseSales(true);
                $products[$productIndex]->setPrice($price - ($price / 100 * $this->sale));
            }
        }
        return $products;

    }

    public function setProductType(string $productType){

        $this->productsTypes[] = $productType;
    }
}

?>