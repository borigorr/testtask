<?php
namespace testTask\interfaces;

interface Product{

    public function __construct(string $productType, float $price);

    public function setProductType(string $productType);

    public function setPrice(float $price);

    public function setUseSales(bool $useSale);

    public function getProductType() : string;

    public function getPrice() : float;

    public function getUseSales() : bool;
}
?>