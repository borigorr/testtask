<?php
namespace testTask\interfaces;

interface Sales{

    public function calcPrice(array $products) : array;

    public function setSale(float $sale);
}
?>