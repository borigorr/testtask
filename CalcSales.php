<?php
namespace testTask;
use testTask\interfaces\{Product, Sales};

class CalcSales{

    private $products = [];

    private $sales = [];

    public function setProduct(Product $product){
        $this->products[] = $product;
    }

    public function setSale(Sales $sale){
        $this->sales[] = $sale;
    }

    public function applySales(){
        foreach ($this->sales as $sale){
            $this->products = $sale->calcPrice($this->products);
        }
    }

    public function getSumm(){
        $sum = 0;
        foreach ($this->products as $product){
            $sum += $product->getPrice();
        }
        return $sum;
    }



}
?>